package com.hap.appchat;

public class Users
{
    public String UserId;
    public String Profilepic;
    public String UserName;
    public String password;

    public Users(String userId, String profilepic, String userName, String password) {
        UserId = userId;
        Profilepic = profilepic;
        UserName = userName;
        this.password = password;
    }

    public Users() {
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getProfilepic() {
        return Profilepic;
    }

    public void setProfilepic(String profilepic) {
        Profilepic = profilepic;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}

